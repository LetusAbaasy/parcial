package Estructura;

public class Afinidad {
	
	private int valor;
	private int personaI;
	private int personaJ;
	
	
	public Afinidad(int valor, int personaI, int personaJ){
		this.valor = valor;
		this.personaI = personaI;
		this.personaJ = personaJ;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public int getPersonaI() {
		return personaI;
	}
	public void setPersonaI(int personaI) {
		this.personaI = personaI;
	}
	public int getPersonaJ() {
		return personaJ;
	}
	public void setPersonaJ(int personaJ) {
		this.personaJ = personaJ;
	}
	

}
