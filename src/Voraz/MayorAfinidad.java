package Voraz;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.text.StyledEditorKit.ForegroundAction;

import Estructura.*;
import Ordenamiento.Arbol;
public class MayorAfinidad {

	public List<ArrayList<Integer>> lista_grupos;
	public Afinidad seleccion(List<Afinidad> candidatos)
	{
		Arbol elArbol = new Arbol();
	    elArbol.setLista(candidatos);
		List<Afinidad> ordenados = elArbol.ordenadosMaxToMin();		
		return ordenados.get(0);
	}
	
	public boolean solucion(List<Integer> solucion, List<Afinidad> candidatos)
	{
		if(solucion.size() == 3 || candidatos.size() == 0){
			return true;
		}
		System.out.println("_________________________________");
		System.out.println("Numero A: "+candidatos.size());
		return false;
	}
	
	public boolean factible(Afinidad x, List<Integer> solucion)
	{		
		if(solucion.size() > 1 && solucion.size() < 3){
			boolean estaPersona1 = false;
			boolean estaPersona2 = false;
			for (Integer persona : solucion) {
				if(persona == x.getPersonaJ()){
					estaPersona1 = true;
				}
				if(persona == x.getPersonaI()){
					estaPersona2 = true;
				}
			}
			
			return (!estaPersona1 && estaPersona2) || (!estaPersona2 && estaPersona1); 
			
		}else if(solucion.size() == 0){
			return true;
		}else{
			return false;
		}
	}
	
	public List<ArrayList<Integer>> voraz(List<Afinidad> candidatos)
	{
		lista_grupos = new ArrayList<ArrayList<Integer>>();
		//Se hace doble ciclo para que siga sacando los demas grupos	
		while(candidatos.size() != 0){
			List<Integer> solucion = new ArrayList<Integer>();
			while(!this.solucion(solucion,candidatos)) {
				Afinidad x = this.seleccion(candidatos);
				candidatos = this.retirar(x, candidatos);
				if(this.factible(x,solucion)){
					solucion = this.insertar(solucion, x);
				}
			}
			lista_grupos.add((ArrayList<Integer>) solucion);
			solucion = new ArrayList<Integer>();
		}
		
		return lista_grupos;
		
		/*if(!this.solucion(solucion,candidatos)) {
			return null;
		}*/
	
				
		
	}
	
	public boolean otroFactible(List<Integer> aretirar, Afinidad x){
		boolean estaPersona1 = false;
		boolean estaPersona2 = false;
		for (Integer persona : aretirar) {
			if(persona == x.getPersonaJ()){
				estaPersona1 = true;
			}
			if(persona == x.getPersonaI()){
				estaPersona2 = true;
			}
		}
		
		return (!estaPersona1 && estaPersona2) || (!estaPersona2 && estaPersona1); 
	}
	

	
	public List<Afinidad> retirar(Afinidad x,List<Afinidad> candidatos)
	{
		System.out.println("Array personas :"+Arrays.asList(candidatos.toString()));
		List<Integer> aretirar = new ArrayList<Integer>();
		aretirar.add(x.getPersonaI());
		aretirar.add(x.getPersonaJ());
		
		List<Afinidad> porretirar = new ArrayList<Afinidad>();
		for (Afinidad xx : candidatos) {
			if(!this.otroFactible(aretirar, xx)){
				porretirar.add(xx);
			}
		}
		
		for (Afinidad afinidad : porretirar) {
			candidatos.remove(afinidad);
		}
			
	
		
		/*List<Afinidad> a_retirar = new ArrayList<Afinidad>(); 
		for (Afinidad afinidad : candidatos) {
			if( (((afinidad.getPersonaI() == x.getPersonaI()) && (afinidad.getPersonaJ() != x.getPersonaJ() )) ||
					((afinidad.getPersonaI() != x.getPersonaI() && (afinidad.getPersonaJ() == x.getPersonaJ())) ) ||
					((afinidad.getPersonaI() == x.getPersonaJ()) && (afinidad.getPersonaJ() != x.getPersonaI() )) || 
					((afinidad.getPersonaI() != x.getPersonaJ() && (afinidad.getPersonaJ() == x.getPersonaI())) ))
					) {
				a_retirar.add(afinidad);				
			}
		}
		for (Afinidad afinidad : a_retirar) {
			candidatos.remove(afinidad);
		}*/
		candidatos.remove(x);
		return candidatos;
	}
	
	public List<Integer> insertar(List<Integer> solucion, Afinidad x)
	{
		
		if(solucion.size() > 0){
			
			List<Integer> agregarTMP = new ArrayList<Integer>();
			
			boolean estaPersona1 = false;			
			
			for (Integer persona : solucion) {
				if(persona == x.getPersonaJ()){
					estaPersona1 = true;
				}				
			}
			
			if(estaPersona1){
				solucion.add(x.getPersonaI());
			}else{
				solucion.add(x.getPersonaJ());
			}
			
		}else{
			solucion.add(x.getPersonaI());
			solucion.add(x.getPersonaJ());
		}
		
		return solucion;
	}
	 
}
