import java.util.List;

public class Grupo {
	private List<Integer> personas;

	public List<Integer> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Integer> personas) {
		this.personas = personas;
	}
	
	public void insertarPersona(int persona)
	{
		this.personas.add(persona);
	}
	
	
}
