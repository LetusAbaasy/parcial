import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import Estructura.Afinidad;
import Voraz.MayorAfinidad;
public class Main {
	
	public static int[][] personas;  
	
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Siendo I la Fila y J la columna, se ingresan los datos de las afinidades en [I,J]\n");
		System.out.println("Ejemplo:\n");
		System.out.println("_______________________");
		System.out.println("P | 1 | 2 | 3 | 4 | 5 | <- esta es I");
		System.out.println("1 | - | f | k | p | u |");
		System.out.println("2 | b | - | l | q | v |");
		System.out.println("3 | c | h | - | r | w |");
		System.out.println("4 | d | i | n | - | x |");
		System.out.println("5 | e | j | o | t | - |");
		System.out.println("^_______________________");
		System.out.println("Esta es J\n\n");
		System.out.println("Ingrese el numero participantes:");
		
        int nPersonas = leer.nextInt();
        
        personas = new int[nPersonas][nPersonas];
        
        List<Afinidad> vectorAfinidades = new ArrayList<Afinidad>();
        
        for (int i = 0; i < nPersonas; i++) {
        	 for (int j = 0; j < nPersonas; j++) {
        		 if(j != i){
        			 System.out.println("Ingrese la afinidad para la persona en posicion i = "+(i+1)+" en j = "+(j+1));
            		 int nAfinidad = leer.nextInt(); 
            		 vectorAfinidades.add(new Afinidad(nAfinidad,i,j));
        		 }        		 
            }
       }
        
        MayorAfinidad vorax = new MayorAfinidad();
        List<ArrayList<Integer>> personas = vorax.voraz(vectorAfinidades);
        System.out.println("_________________________________");
        System.out.println("Grupos en el array:\n");
        System.out.println(personas.toString());
        System.out.println("______________ FIN ______________");
       /*Arbol elArbol = new Arbol();
       elArbol.setLista(vectorAfinidades);
       
       
       //Se obtiene de menor a mayor ya que en el iterator se esta recorriendo la lista del ultimo elemento al 
       //primero
       List<Afinidad> ordenados = elArbol.ordenadosMaxToMin();
       
       ListIterator<Afinidad> iterador = ordenados.listIterator(ordenados.size());
       int nGrupo = 0;
       
       ArrayList<Integer> grupoAnterior  = null;
       
       List<ArrayList<Integer>> listagrupos = new ArrayList<ArrayList<Integer>>();
       
       for(int l = 0; l < ((nPersonas * nPersonas) - nPersonas); l++){
    	 //Se recorre los elemenos del ultimo al primero
    	   Afinidad afinidadTMP = ordenados.get(l);
    	   if(grupoAnterior == null){
    		   grupoAnterior = new ArrayList<Integer>();
    	   }
    	  int valPerI = afinidadTMP.getPersonaI();
   		  int valPerJ = afinidadTMP.getPersonaJ();
    	   if(grupoAnterior.size() < 3 && grupoAnterior.size() > 0){
    		   for (int i = 0; i < grupoAnterior.size(); i++) {
    			   int persona = grupoAnterior.get(i);
					if(persona == valPerI){
						 grupoAnterior.add(valPerI);
					}else if(persona == valPerJ){
						grupoAnterior.add(valPerJ);
					}
    		   }   		  
    	   }else{
    		   
    		   listagrupos.add(grupoAnterior);
    	   }
    	   grupoAnterior = null;
       }*/
	}

}
