package Ordenamiento;
/*
 * Es es el esqueleto del arbol, se encargara de almacenar el objeto 
 * y podra tener un hijo su izquierda que es otro objeto de clase nodo asi se ve graficamente y que correponde
 * a una llave menor que su llave, un un hijo derecho que representa otro nodo que 
 * su llave es mayor a su llave y por ultimo tiene un padre que representa otro nodo dode donde 
 * este viene
 */

import Estructura.Afinidad;

/**
 *
 * @author Juan Diaz - Furiosojack <furiosojuan0@gmail.com>
 */
public class Nodo {
    
    private Nodo hijoDer;
    private Nodo hijoIzq;
    private Nodo padre;
    private Afinidad afinidad;
    private int key;

    public Nodo(int key)
    {
        this.key = key;
        this.hijoDer = null;
        this.hijoIzq = null;
    }
    public Nodo getHijoDer() {
        return hijoDer;
    }

    public void setHijoDer(Nodo hijoDer) {
        this.hijoDer = hijoDer;
    }

    public Nodo getHijoIzq() {
        return hijoIzq;
    }

    public void setHijoIzq(Nodo hijoIzq) {
        this.hijoIzq = hijoIzq;
    }

    public Nodo getPadre() {
        return padre;
    }

    public void setPadre(Nodo padre) {
        this.padre = padre;
    }

    public Afinidad getValor() {
        return afinidad;
    }

    public void setValor(Afinidad afinidad) {
        this.afinidad = afinidad;
    }

    public int getKey() {
        return key;
    }
}