package Ordenamiento;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import Estructura.Afinidad;

/**
 *
 * @author Juan Diaz - Furiosojack <furiosojuan0@gmail.com>
 */
public class Arbol {

    private Nodo raiz;

    public Nodo getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }

    public void insertar(int key, Afinidad afinidad)
    {
        //Crear nodo actual a insertar
        Nodo nodoAInsertar = new Nodo(key);
        nodoAInsertar.setValor(afinidad);

        //Validar si el nodo temporal es el nodo raiz
        if(this.raiz == null){
            this.raiz = nodoAInsertar;
            //termina
            return;
        }

        //Creo un nodo temporal
        ///Corresponde al nodo raiz

        Nodo nodoUbicacionAcutal = this.getRaiz();

        //Se ubica el lugar donde se va a insertar el nodo actual

        while(nodoUbicacionAcutal != null){
            //Se dice que el nodo actual tiene com padre al nodo temporal
            nodoAInsertar.setPadre(nodoUbicacionAcutal);

            //Se escala en el arbol
            //Se decide si se va por la izquierda o por la derecha el nodo actual

            if(nodoAInsertar.getKey() >= nodoUbicacionAcutal.getKey()){
                //ahora la nueva posicion es el hijo derecho del nodo actual es decir
                //bajo en el arbol por la derecha
                nodoUbicacionAcutal = nodoUbicacionAcutal.getHijoDer();

            }else{
                nodoUbicacionAcutal = nodoUbicacionAcutal.getHijoIzq();
            }
        }
         //Se decide la ubicacion del nodo nuevo a insertar
        if(nodoAInsertar.getKey() > nodoAInsertar.getPadre().getKey()){
            nodoAInsertar.getPadre().setHijoDer(nodoAInsertar);
        }else{
            nodoAInsertar.getPadre().setHijoIzq(nodoAInsertar);
        }


    }

    private boolean existeRaiz()
    {
        return this.raiz != null;
    }

    private void inOrden(Nodo nodo, List<Afinidad> ordenados)
    {
      if(nodo != null){
        inOrden(nodo.getHijoIzq(), ordenados);
         
        ordenados.add(nodo.getValor());
        inOrden(nodo.getHijoDer(),ordenados);
      }
    }

    private void inDisOrden(Nodo nodo, List<Afinidad> ordenados)
    {
      if(nodo != null){
        inDisOrden(nodo.getHijoDer(),ordenados);
        ordenados.add(nodo.getValor());
        inDisOrden(nodo.getHijoIzq(),ordenados);
      }
    }

    public List<Afinidad> ordenadosMinToMax()
    {
        List<Afinidad> ordenados = new ArrayList<Afinidad>();
        this.inOrden(this.getRaiz(), ordenados);
        return ordenados;
    }

    public List<Afinidad> ordenadosMaxToMin()
    {
      List<Afinidad> ordenados = new ArrayList<Afinidad>();
      this.inDisOrden(this.getRaiz(),ordenados);
      return ordenados;
    }
    
    public void setLista(List<Afinidad> lista)
    {
      ListIterator<Afinidad> iterador = lista.listIterator(lista.size());
      while(iterador.hasPrevious()){
    	  Afinidad afinidadTMP =  iterador.previous();
        //Sacar el el precio que se gasta comprando esta probabilidad        
        this.insertar(afinidadTMP.getValor(),afinidadTMP);
      }
    }
    
    
}